// Modules
const express = require('express');
const UserResources = express.Router();

// Controllers
const { UserControllers } = require('../controllers');

// All user resources
UserResources.get('/', UserControllers.getAll);
UserResources.post('/', UserControllers.createUser);
UserResources.get('/:guid', UserControllers.getByGuid);
UserResources.put('/:guid', UserControllers.updateUser);
UserResources.delete('/:guid', UserControllers.deleteUser);

module.exports = UserResources;
