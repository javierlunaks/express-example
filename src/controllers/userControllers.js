// Models
const { User } = require('../models');

// Fecth all users
const getAll = (req, res) => {
  User.getAll((users) => {
    res.send(users);
  });
};

// Get user by guid
const getByGuid = (req, res) => {
  const { guid } = req.params;
  // Read all user
  User.getAll((users) => {
    // Filter by guid
    const user = users.find(ent => ent.guid === guid);

    if (user) {
      res.send(user);
    } else {
      res.status(404).send({
        message: 'Ups!!! User not found.',
      });
    }
  });
};

// Add new user to users
const createUser = (req, res) => {
  const { body } = req;
  // Create new instance
  const newUser = new User(body);
  // Save in db
  newUser.save();
  res.send({
    message: 'User successfully created!!!',
    guid: newUser.getGuid(),
  });
};

// Update an existing user
const updateUser = (req, res) => {
  const { params: { guid }, body } = req;
  // Read all user
  User.getAll((users) => {
    // Filter by guid
    const user = users.find(ent => ent.guid === guid);

    if (user) {
      Object.assign(user, body);
      User.update(users);
      res.send({
        message: 'User successfully updated!!!',
      });
    } else {
      res.status(404).send({
        message: 'Ups!!! User not found.',
      });
    }
  });
};

// Delete user from users
const deleteUser = (req, res) => {
  const { guid } = req.params;
  // Read all user
  User.getAll((users) => {
    // Filter by guid
    const userIdx = users.findIndex(ent => ent.guid === guid);

    if (userIdx !== -1) {
      users.splice(userIdx, 1);
      User.update(users);
      res.send({
        message: 'User successfully deleted!!!',
      });
    } else {
      res.status(404).send({
        message: 'Ups!!! User not found.',
      });
    }
  });
};

module.exports = {
  getAll,
  getByGuid,
  createUser,
  updateUser,
  deleteUser,
};
