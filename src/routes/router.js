// Modules
const express = require('express');
const router = express.Router();

// Resources
const { UserResources } = require('../resources');

// All routes
router.use('/users', UserResources);

module.exports = router;
