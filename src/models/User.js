// Modules
const fs = require('fs');
const path = require('path');
const uuid = require('uuid');

// Path to users.json
const p = path.join(path.dirname(require.main.filename), 'data', 'users.json');

module.exports = class User {
  constructor(data) {
    const { name, age } = data;
    this.name = name;
    this.age = age;
    this.guid = uuid.v4();
  }

  getGuid() {
    return this.guid;
  }

  // We push a new user to users array and save
  save() {
    // We read the file everytime we need to modify it
    fs.readFile(p, (err, data) => {
      let users = [];
      if (!err) {
        users = JSON.parse(data);
      }
      users.push(this);
      // Write the file
      fs.writeFile(p, JSON.stringify(users), (err) => console.log(err));
    })
  }

  // We update data with the given one
  static update(users) {
    fs.writeFile(p, JSON.stringify(users), (err) => console.log(err));
  }

  // get and parse the data (async)
  static getAll(cb) {
    fs.readFile(p, (err, data) => {
      let users = [];
      if (!err) {
        users = JSON.parse(data);
      }
      // callback function when the data is ready
      cb(users);
    });
  }
};
